﻿using UnityEngine;
using System.Collections;

public class LanternButton : MonoBehaviour {
	public Light lantern;
	public GUITexture button;
	private float _currBattery;
	private float _maxCharge;
	private float _menu;
	private Config _config;
	void Start(){
		_maxCharge = 60;
		_currBattery = _maxCharge;
		_menu = 0;
		GameObject _temp = GameObject.FindGameObjectWithTag("UnDestroy");
		_config = (Config)_temp.GetComponent<Config>();
	}

	void Update(){
		if (_currBattery > 0 && lantern.enabled == true){
			_currBattery -= Time.deltaTime;
		}
		if (_currBattery < 0){
			_currBattery = 0;
			lantern.enabled = false;
		}
		int count = Input.touchCount;
		for (int i = 0; i < count; i++) {
			Touch touch = Input.GetTouch(i);
			if (button.HitTest(touch.position) && (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary )){
				//Debug.Log("I am Moved!");
				_menu += Time.deltaTime;
			}
			if (button.HitTest(touch.position) && touch.phase == TouchPhase.Ended){
				if (_menu > 2){
					Application.LoadLevel(_config.getSceneAfterDead());
				} else {
					_menu = 0;
				}
				if (_currBattery > 0){
						lantern.enabled = !lantern.enabled;
				}
				if (_currBattery <= 0 && GameSettings._Instance.getBattery() > 0){
					GameSettings._Instance.putBatteryOnLantern();
					_currBattery = _maxCharge;
					lantern.enabled = !lantern.enabled;
				}
			}
		}
	}

}
