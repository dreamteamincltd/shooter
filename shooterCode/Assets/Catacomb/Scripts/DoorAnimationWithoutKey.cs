﻿using UnityEngine;
using System.Collections;

public class DoorAnimationWithoutKey : MonoBehaviour, IDoorAnimation {

	public AnimationClip _aOpen;
	public AnimationClip _aClose;
	public GameObject _door;
	
	public void Start () {
		
	}
	
	public void Update () {
		
	}
	
	public void OnTriggerEnter(Collider temp){
		if (temp.tag == "Player"){
			_door.animation.Play(_aOpen.name);
		}
	}
	
	public void OnTriggerExit(Collider temp){
		if (temp.tag == "Player"){
			_door.animation.Play(_aClose.name);
		}
	}
}
