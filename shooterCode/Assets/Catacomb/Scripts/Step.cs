﻿using UnityEngine;
using System.Collections;

public class Step : MonoBehaviour {
	private CharacterController _controller;
	public AudioClip[] _stepAudio;
	private float _speed;
	private UserStats _stats;
	//private UserStats _stats;
	// Use this for initialization
	void Awake () {
		_controller = (CharacterController)this.GetComponent("CharacterController");
		_speed = 0.65f;
		_stats = (UserStats)GameObject.FindGameObjectWithTag("Player").GetComponent("UserStats");
	}
	
	IEnumerator Start(){
		while (true) {
			if (_controller.isGrounded && _controller.velocity.magnitude > 3 && _stats.getCurHealth() > 1){
				audio.PlayOneShot(_stepAudio[Random.Range(0, _stepAudio.Length - 1)], 0.5f);
				yield return new WaitForSeconds(_speed);	
			} else {
				yield return 0;
			}
		}
	}
}
