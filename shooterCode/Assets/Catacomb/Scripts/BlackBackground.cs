﻿using UnityEngine;
using System.Collections;

public class BlackBackground : MonoBehaviour {
	private Color _color = Color.black;
	private float _speed = 0;
	private Texture _texture;
	private float _fill;
	private Config _config;

	void Start () {
		Texture2D temp = new Texture2D(1, 1) as Texture2D;
		temp.SetPixel (0, 0, _color);
		temp.Apply ();
		_texture = temp;
		GameObject _temp = GameObject.FindGameObjectWithTag("UnDestroy");
		_config = (Config)_temp.GetComponent<Config>();
	}

	public void setSpeed(float x){
		_speed = x;
	}

	public float getSpeed(float x){
		return _speed;
	}

	void Update () {
		_color.a = _fill;
		if (_fill >= 1){
			Application.LoadLevel(_config.getSceneAfterDead());
			//_fill = 1;
		}
		if (_fill < 0){
			_fill = 0;
		}
		_fill += Time.deltaTime*_speed;
	}

	void OnGUI(){
		GUI.color = _color;
		GUI.DrawTexture(new Rect (0, 0, Screen.width, Screen.height), _texture, ScaleMode.StretchToFill, true);
	}
}
