﻿using UnityEngine;
using System.Collections;

public class ZombyAI : MonoBehaviour, IEnemyAI {
	private NavMeshAgent _agent;
	private float _speed;
	public Transform _target;
	private GameObject _player;
	private int _hearing; // предел слышимости
	private Transform _home;
	private UserStats _stats;
	private float _attackTimer;
	private float _coolDown;
	private int _damage;
	private float _maxAngle;
	private float _angle;
	public float _turnSpeed = 90;
	
	public void Start () {
		_speed = 3;
		_agent = gameObject.GetComponent<NavMeshAgent>();
		_agent.speed = _speed;
		_player = GameObject.FindGameObjectWithTag("Player");
		_stats = (UserStats)_player.GetComponent("UserStats");
		_damage = 51;
		_coolDown = 1.2f;
		_attackTimer = _coolDown;
	}

	public void Awake(){
		_home = transform.parent.transform;
	}
	
	public void Update () {
		if (Vector3.Distance(_agent.transform.position, _player.transform.position) < 50) {
			animation.CrossFade("walk1");
			_target = _player.transform;
		} else {
			_target = _home;
		}
		if (Vector3.Distance(_agent.transform.position, _player.transform.position) < 5){
			_agent.speed = 0;
			animation.CrossFade("attack1");
			_maxAngle = _turnSpeed * Time.deltaTime;
			_angle = Vector3.Angle(this.transform.forward, (_player.transform.position - this.transform.position).normalized);
			Quaternion rot = Quaternion.LookRotation(_player.transform.position - this.transform.position);
			if (_maxAngle < _angle){
				this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rot, _maxAngle/_angle);
			} else {
				this.transform.rotation = rot;
			}
			if (_attackTimer > 0){
				_attackTimer -= Time.deltaTime;
			}
			if (_attackTimer < 0){
				_attackTimer = 0;
			}
			if (_attackTimer == 0){
				_stats.decCurHealth(_damage);
				_attackTimer = _coolDown;
			}

		} else {
			_attackTimer = _coolDown;
			_agent.speed = _speed;
		}
		if (Vector3.Distance (_agent.transform.position, _home.position) < 0.5){
			animation.CrossFade("idle");
		}
		_agent.SetDestination (_target.position);
	}

}