﻿using UnityEngine;
using System.Collections;

public class DamageFromAIToPlayer : MonoBehaviour {
	private int _damage;
	private UserStats _stats;
	void Start () {
		_stats = (UserStats)GameObject.FindGameObjectWithTag("Player").GetComponent("UserStats");
		_damage = 10;
	}

	void Update () {
	
	}

	void OnTriggerEnter(Collider obj){
		if (obj.collider.tag == "Player") {
			_stats.decCurHealth(_damage);
		}
	}	
}
