﻿using UnityEngine;
using System.Collections;

public class Config : MonoBehaviour {

	private static string[] _st = {"MenuScene", "LoadingScene", "FirstLevelScene", "LoadingScene","SecondLevelScene"};
	private int _i = 0;
	void Awake(){
		DontDestroyOnLoad(this);
	}
	public string getNextScene(){
		_i++;
		return _st[_i];
	}

	public string getSceneAfterDead(){
		_i = -1;
		return getNextScene();
	}
}
