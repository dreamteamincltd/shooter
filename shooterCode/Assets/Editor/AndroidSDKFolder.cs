﻿using UnityEditor;

public static class AndroidSDKFolder
{
	static string Path
	{
		get { return EditorPrefs.GetString("AndroidSdkRoot"); }
		set { EditorPrefs.SetString("AndroidSdkRoot", value); }
	}
}