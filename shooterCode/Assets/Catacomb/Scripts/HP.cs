﻿using UnityEngine;
using System.Collections;

public class HP : MonoBehaviour {
	public GUISkin _skin;
	private bool _isVisible = true;
	private UserStats _stats;
	private GUIStyle _style;
	private PlaySound _sound;

	void Start () {
		_stats = (UserStats)this.GetComponent("UserStats");
		_style = new GUIStyle();
		_style.fontSize = 60;
		_style.fontStyle = FontStyle.Bold;
		_sound = (PlaySound)GameObject.FindGameObjectWithTag("ForMusic").GetComponent("PlaySound");
	}

	void Update () {
		
	}

	void OnGUI(){
		if (_isVisible == true) {
			GUI.skin = _skin;
			float maxHealth = (float)_stats.getMaxHealth();
			float curHealth = (float)_stats.getCurHealth();
			float healthBar = curHealth / maxHealth;
			GUI.Box(new Rect(10, 10, 200, 20), " ", GUI.skin.GetStyle("BackGround"));
			GUI.Box(new Rect(10, 10, 200*healthBar, 20), " ", GUI.skin.GetStyle("HPBar"));
			GUI.Box(new Rect(10, 10, 200, 20), " ", GUI.skin.GetStyle("Border"));
			GUI.Label(new Rect(10, 30, 200, 20), " Battery: " + System.Convert.ToString(GameSettings._Instance.getBattery()));
			if (curHealth < 1){
				_sound.pauseSound();
				GUI.color = Color.white;
				GUI.Label(new Rect((Screen.width/2) -100, (Screen.height/2), 500, 300), "Wasted", _style);
			}
		}
	}
}