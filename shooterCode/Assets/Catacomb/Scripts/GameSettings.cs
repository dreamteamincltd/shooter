﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour{
	public static GameSettings _Instance{ get; private set;}
	private bool _isLocatedInBag;
	private int _battery;

	void Awake(){
		_Instance = this;
		_battery = 3;
		_isLocatedInBag = false;
	}

	public void incBattery(){
		_battery++;
	}

	public int putBatteryOnLantern(){
		_battery--;
		if (_battery < 0){
			_battery = 0;
		}
		return _battery;
	}

	public int getBattery(){
		return _battery;
	}

	public void setKey(){
		_isLocatedInBag = true;
	}

	public bool getStateOfKey(){
		return _isLocatedInBag;
	}

}
