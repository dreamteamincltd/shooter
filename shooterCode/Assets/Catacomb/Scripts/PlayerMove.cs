﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	private CharacterController _controller;
	public AnimationClip _stepAnimation;

	void Start () {
		_controller = (CharacterController)GameObject.FindGameObjectWithTag("Player").GetComponent("CharacterController");
	}
	

	void Update () {
		if (_controller.isGrounded && _controller.velocity.magnitude > 3){
			animation.clip = _stepAnimation;
			animation.Play();
		} 
	}
}
