﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {
	private bool menuFl = true;
	private bool settingFl = false;
	private Config _config;

	void Start () {
		GameObject _temp = GameObject.FindGameObjectWithTag("UnDestroy");
		_config = (Config)_temp.GetComponent<Config>();
	}

	void Update () {
		
	}

	void OnGUI(){
		if (menuFl) {
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2), 75, 40), "New Game")){
				Application.LoadLevel(_config.getNextScene());
			}
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2) + 50, 75, 40), "Settings")){
				menuFl = false;
				settingFl = true;
			}
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2)+100, 75, 40), "Exit")){
				Application.Quit();
			}
		}

		if (settingFl) {
			//GUI.Label(new Rect((Screen.width/2) - 30, (Screen.height/2), 75, 40), "Quality Settings:"); 
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2), 75, 40), "Maximum")){
				QualitySettings.SetQualityLevel(4);
			}
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2) + 50, 75, 40), "Normal")){
				QualitySettings.SetQualityLevel(3);
			}
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2) + 100, 75, 40), "Low")){
				QualitySettings.SetQualityLevel(1);
			}
			if (GUI.Button(new Rect((Screen.width/2) -30, (Screen.height/2) + 150, 75, 40), "Back")){
				menuFl = true;
				settingFl = false;
			}
		}
	}
}
