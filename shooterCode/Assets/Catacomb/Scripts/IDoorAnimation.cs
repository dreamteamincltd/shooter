﻿using UnityEngine;
using System.Collections;

interface IDoorAnimation{	

	void Start();

	void Update();

	void OnTriggerEnter(Collider temp);

	void OnTriggerExit(Collider temp);
}
