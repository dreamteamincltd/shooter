﻿using UnityEngine;
using System.Collections;

public class LoadNextLevel : MonoBehaviour {
	private Config _config;

	void Start () {
		GameObject _temp = GameObject.FindGameObjectWithTag("UnDestroy");
		_config = (Config)_temp.GetComponent<Config>();
	}

	void OnTriggerEnter(Collider temp){
		string _st = _config.getNextScene ();
		Application.LoadLevel (_st);
	
	}
}
