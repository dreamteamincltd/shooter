﻿using UnityEngine;
using System.Collections;

public class BatteryCs : MonoBehaviour {
	public GameObject _player;

	void Start (){
		_player = GameObject.FindGameObjectWithTag("Player");
	}

	void OnMouseDown(){
		if (Vector3.Distance (transform.position, _player.transform.position) <= 10) {
			GameSettings._Instance.incBattery();
			Destroy (this.gameObject);
		}
	}
}
