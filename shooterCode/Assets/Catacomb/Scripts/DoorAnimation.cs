﻿using UnityEngine;
using System.Collections;

public class DoorAnimation : MonoBehaviour, IDoorAnimation {
	public AnimationClip _aOpen;
	public AnimationClip _aClose;
	public GameObject _door;
	private GUIStyle _style;
	private bool _bState;


	public void Start(){
		_style = new GUIStyle();
		_style.fontSize = 60;
		_style.fontStyle = FontStyle.Bold;
		_bState = false;
	}

	public void Update () {
	
	}

	public void OnTriggerEnter(Collider temp){
		if (temp.tag == "Player" && GameSettings._Instance.getStateOfKey() == true){
			_door.animation.Play(_aOpen.name);
		}
		if (temp.tag == "Player" && GameSettings._Instance.getStateOfKey() == false){
			_bState = true;
		}


	}

	public void OnTriggerExit(Collider temp){
		if (temp.tag == "Player" && GameSettings._Instance.getStateOfKey() == true){
			_door.animation.Play(_aClose.name);
		}
		_bState = false;
	}

	void OnGUI(){
		if (_bState == true){
			GUI.Label(new Rect((Screen.width/2) -150, (Screen.height/2), 500, 300), "Door is Closed", _style);
		}
	}
}
