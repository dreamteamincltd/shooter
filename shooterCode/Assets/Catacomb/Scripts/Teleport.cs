﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	public GameObject obj;

	void OnTriggerEnter(Collider _obj){
		if (_obj.CompareTag ("Player")){
			_obj.transform.position = obj.transform.position;
		}
	}
}
