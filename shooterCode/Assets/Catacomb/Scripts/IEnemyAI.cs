﻿using UnityEngine;
using System.Collections;

interface IEnemyAI{

	void Start();

	void Update();

	void Awake();
}
