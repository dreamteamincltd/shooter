﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {
	public AudioSource _source;
	private AudioClip _clip;

	public void playSound(AudioClip sound){
		_source.PlayOneShot(sound);
	}

	public void pauseSound(){
		_source.Stop();
	}
}
