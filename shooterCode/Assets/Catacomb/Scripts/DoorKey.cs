﻿using UnityEngine;
using System.Collections;

public class DoorKey : MonoBehaviour{
	public Texture2D _hand;
	static public bool b;
	public GameObject _player;
	void Start (){
		_player = GameObject.FindGameObjectWithTag("Player");
	}

	void Update (){
	
	}

	void OnMouseDown(){
		if (Vector3.Distance (transform.position, _player.transform.position) <= 10) {
			GameSettings._Instance.setKey();
			Destroy (this.gameObject);
		}
	}

}
