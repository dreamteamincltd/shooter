﻿using UnityEngine;
using System.Collections;

public class StatueAI : MonoBehaviour, IEnemyAI {
	private GameObject _player;
	private Transform _transform;
	private float _attackTimer;
	private float _coolDown;
	private int _damage;
	private float _maxAngle;
	private float _angle;
	public float _turnSpeed = 90;
	private UserStats _stats;
	private float _y;
	private bool _bAudio;
	private PlaySound _sound;
	public AudioClip _laugh;

	public void Start () {
		_player = GameObject.FindGameObjectWithTag("Player");
		_transform = this.transform;
		_coolDown = 2;
		_damage = 26;
		_coolDown = 5.0f;
		_attackTimer = _coolDown;
		_stats = (UserStats)_player.GetComponent("UserStats");
		_y = _transform.position.y;
		_bAudio = false;
		_sound = (PlaySound)GameObject.FindGameObjectWithTag("ForMusic").GetComponent("PlaySound");
	}

	public void Awake(){

	}

	public void Update (){
		if (Vector3.Distance(_transform.position, _player.transform.position)<20){
			_maxAngle = _turnSpeed * Time.deltaTime;
			_angle = Vector3.Angle(this.transform.forward, (_player.transform.position - this.transform.position).normalized);
			Quaternion rot = Quaternion.LookRotation(_player.transform.position - this.transform.position);
			rot.Set(0, rot.y, 0, rot.w);
			if (_maxAngle < _angle){
				this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rot, _maxAngle/_angle);
			} else {
				this.transform.rotation = rot;
			}
			_transform.position = new Vector3(_transform.position.x, _y, _transform.position.z);
			if (_attackTimer > 0){
				_attackTimer -= Time.deltaTime;
			}
			if (_attackTimer < 0){
				_attackTimer = 0;
			}
			if (_attackTimer == 0 && _angle < 25){
				//playSound();
				_stats.decCurHealth(_damage);
				_attackTimer = _coolDown;
				if (_bAudio == false){
					_bAudio = true;
					_sound.playSound(_laugh);
					//playSound();
				}

			}
		} else {
			_attackTimer = _coolDown;
		}
	}
}
