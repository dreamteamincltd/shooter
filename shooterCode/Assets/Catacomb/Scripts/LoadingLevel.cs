﻿using UnityEngine;
using System.Collections;

public class LoadingLevel : MonoBehaviour {
	public Texture _texture;
	private AsyncOperation _asOp;
	private float _fCurLoad;
	private int _curLoad;
	private Config _config;

	void Start () {
		_fCurLoad = 0;
		GameObject _temp = GameObject.FindGameObjectWithTag("UnDestroy");
		_config = (Config)_temp.GetComponent<Config>();
		_asOp = Application.LoadLevelAsync(_config.getNextScene());
		_asOp.allowSceneActivation = false;
	}

	void OnGUI(){
		if (_fCurLoad <= 100){
			_fCurLoad += Time.deltaTime*10;
			_curLoad = Mathf.RoundToInt(_fCurLoad);
		}

		if (_asOp.progress < 1 && _curLoad == 100) {
			_asOp.allowSceneActivation = true;
		}

		if (_asOp.progress != null){
			GUI.Label(new Rect(Screen.width/2 - 65, Screen.height/2 + 220, Screen.width, Screen.height), "Загрузка: " + _curLoad);
			GUI.DrawTexture(new Rect(0, Screen.height-60, _fCurLoad/100*Screen.width, 40), _texture);

		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
