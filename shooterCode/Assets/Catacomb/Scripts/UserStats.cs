﻿using UnityEngine;
using System.Collections;

public class UserStats : MonoBehaviour {
	private BlackBackground _blackBackScript;
	private float _fillSpeed = 0.5f; // Скорость затемнения экрана во время смерти игрока
	private Config _config;
	private int _maxHealth;
	private int _curHealth;
	private PlaySound _sound;
	public AudioClip _clip;
	// Use this for initialization
	void Start () {
		_blackBackScript = (BlackBackground)this.GetComponent("BlackBackground");
		_sound = (PlaySound)GameObject.FindGameObjectWithTag("ForMusic").GetComponent("PlaySound");
		_maxHealth = 100;
		_curHealth = 100;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int getMaxHealth(){
		return _maxHealth;
	}

	public int getCurHealth(){
		return _curHealth;
	}
	public void decCurHealth(int x){
		_curHealth -= x;
		_sound.playSound(_clip);
		if (_curHealth < 0){
			_blackBackScript.setSpeed(_fillSpeed);
			this.GetComponentInChildren<CharacterController>().enabled = false;
			_curHealth = 0;
		}
	}
}
